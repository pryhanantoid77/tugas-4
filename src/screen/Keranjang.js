import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  Image,
  TouchableOpacity,
  FlatList,
} from 'react-native';
import React, {useEffect, useState} from 'react';
import {COLORS} from '../constant';
import Icon from 'react-native-vector-icons/dist/Ionicons';
import Iconn from 'react-native-vector-icons/dist/Feather';
import {KERANJANG} from '../constant/data';

const Keranjang = ({navigation, route}) => {
  console.log('1234 route', route);
  const [data, setData] = useState([]);

  useEffect(() => {
    console.log('1234 route 2', route);
    const formData = route?.params ?? null;
    if (formData) {
      const dataKeranjang = [...KERANJANG, formData];
      console.log('1234 keranjang', dataKeranjang);
      setData(dataKeranjang);
    } else {
      setData(KERANJANG);
    }
  }, [route]);

  const SliderItem = ({item}) => {
    return (
      <View style={styles.card}>
        <Image
          source={{uri: item.img}}
          style={{marginLeft: 10, height: 100, width: 100}}
        />
        <View style={styles.text}>
          <Text style={{color: COLORS.black, fontWeight: 'bold'}}>
            {item.merk} - {item.color} - {item.size}
          </Text>
          <Text style={{marginTop: 10}}>{item.checkbox}</Text>
          <Text style={{marginTop: 10}}>Note : {item.note}</Text>
        </View>
      </View>
    );
  };

  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <Icon
          name="arrow-back"
          size={22}
          color="black"
          onPress={() => navigation.goBack()}
        />
        <Text
          style={{
            color: COLORS.black,
            marginLeft: 5,
            fontSize: 18,
            fontWeight: 'bold',
          }}>
          Keranjang
        </Text>
      </View>
      <View style={styles.body}>
        <ScrollView
          showsVerticalScrollIndicator={false}
          contentContainerStyle={{paddingBottom: 20}}>
          <FlatList
            data={data}
            renderItem={({item}) => <SliderItem item={item} />}
          />
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <TouchableOpacity
              style={{marginTop: 50, width: 130}}
              onPress={() => navigation.navigate('FormPemesanan')}>
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                <Iconn name="plus-square" size={22} color={COLORS.primary} />
                <Text
                  style={{
                    color: COLORS.primary,
                    fontWeight: 'bold',
                    marginLeft: 5,
                  }}>
                  Tambah Barang
                </Text>
              </View>
            </TouchableOpacity>
          </View>
        </ScrollView>
      </View>
      <View style={{flex: 1, paddingHorizontal: 22, alignItems: 'center'}}>
        <TouchableOpacity
          style={styles.bottomconfirm}
          onPress={() => navigation.navigate('Checkout')}>
          <Text style={styles.textbottom}>Selanjutnya</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default Keranjang;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLORS.silver,
  },
  header: {
    height: 56,
    width: '100%',
    backgroundColor: COLORS.white,
    alignItems: 'center',
    flexDirection: 'row',
    paddingHorizontal: 22,
    shadowColor: COLORS.black,
    shadowOffset: {
      height: 2,
      width: 0,
    },
    shadowOpacity: 0.25,
    elevation: 5,
  },
  body: {
    paddingHorizontal: 22,
    // paddingTop: 10,
  },
  card: {
    backgroundColor: COLORS.white,
    height: 135,
    width: '100%',
    flexDirection: 'row',
    marginTop: 10,
    alignItems: 'center',
    borderRadius: 10,
  },
  text: {
    marginLeft: 10,
  },
  bottomconfirm: {
    marginTop: 30,
    backgroundColor: COLORS.primary,
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 8,
    paddingVertical: 15,
    bottom: 0,
    position: 'absolute',
    marginBottom: 20,
  },
  textbottom: {
    color: COLORS.white,
    fontSize: 16,
    fontWeight: 'bold',
  },
});
