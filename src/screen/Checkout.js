import {
  FlatList,
  Image,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import React, {useState} from 'react';
import {COLORS} from '../constant';
import {CheckBox} from 'react-native-elements';
import Icon from 'react-native-vector-icons/dist/Ionicons';
import {listPembayaran} from '../constant/data';

const Checkout = ({navigation}) => {
  const [isSelected, setSelection] = useState('');
  // const SliderItem = ({item}) => {
  //   return (
  //     <TouchableOpacity onPress={() => setSelection(item.title)}>
  //       <View
  //         style={{
  //           backgroundColor: isSelected == item.title ? '#03426229' : 'white',
  //           height: 82,
  //           width: 162,
  //           padding: 20,
  //           alignItems: 'center',
  //           justifyContent: 'center',
  //           marginTop: 10,
  //           borderColor: isSelected ? '#034262' : '#E1E1E1',
  //           borderWidth: 1,
  //           marginHorizontal: 5,
  //           borderRadius: 10,
  //         }}>
  //         {isSelected == item.title ? (
  //           <View style={{position: 'absolute', right: 0, top: 0}}>
  //             <Icon name="ios-checkmark-circle" size={22} color="#034262" />
  //           </View>
  //         ) : null}
  //         <Image source={item.img} />
  //         <Text>{item.title}</Text>
  //       </View>
  //     </TouchableOpacity>
  //   );
  // };

  return (
    <View style={styles.container}>
      <ScrollView
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{paddingBottom: 20}}>
        <View style={styles.customer}>
          <Text>Data Customer</Text>
          <Text style={{color: COLORS.black, marginVertical: 5}}>
            Agil Bani (0813763476)
          </Text>
          <Text style={{color: COLORS.black, marginBottom: 2}}>
            Jl. Perumnas, Condong catur, Sleman, Yogyakarta
          </Text>
          <Text style={{color: COLORS.black}}>gantengdoang@dipanggang.com</Text>
        </View>
        <View style={styles.outlet}>
          <Text>alamat Outlet Tujuan</Text>
          <Text style={{color: COLORS.black, marginVertical: 5}}>
            Jack repair - Seturan (0813763476)
          </Text>
          <Text style={{color: COLORS.black, marginBottom: 2}}>
            Jl. Afandi No 18, Sleman, Yogyakarta
          </Text>
        </View>
        <View
          style={{backgroundColor: COLORS.white, marginTop: 10, padding: 20}}>
          <Text>Barang</Text>
          <View style={styles.barang}>
            <Image
              source={require('../assets/image/sepatukeranjang.png')}
              style={{marginLeft: 10}}
            />
            <View style={{marginLeft: 10}}>
              <Text style={{color: COLORS.black, fontWeight: 'bold'}}>
                New Balance - Pink Abu - 40
              </Text>
              <Text style={{marginTop: 10}}>Cuci Sepatu</Text>
              <Text style={{marginTop: 10}}>Note : -</Text>
            </View>
          </View>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              marginTop: 10,
            }}>
            <Text style={{color: COLORS.black}}>1 Barang</Text>
            <Text style={{color: COLORS.black, fontWeight: 'bold'}}>
              @50.000
            </Text>
          </View>
        </View>
        <View style={styles.rincian}>
          <Text>Rincian Pembayaran</Text>
          <View
            style={{
              flexDirection: 'row',
              //   justifyContent: 'space-between',
              //   backgroundColor: 'aqua',
              //   width: '100%',
            }}>
            <Text style={{color: COLORS.black, width: '25%'}}>Cuci Sepatu</Text>
            <Text style={{color: '#FFC107', width: '30%'}}>1x Pasang</Text>
            <Text
              style={{color: COLORS.black, width: '45%', textAlign: 'right'}}>
              Rp 30.000
            </Text>
          </View>
          <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
            <Text style={{color: COLORS.black}}>Biaya Antar</Text>
            <Text style={{color: COLORS.black}}>Rp 3.000</Text>
          </View>
          <View
            style={{backgroundColor: '#EDEDED', height: 1, marginVertical: 5}}
          />
          <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
            <Text style={{color: COLORS.black}}>Total</Text>
            <Text style={{color: COLORS.black}}>Rp 33.000</Text>
          </View>
        </View>
        <View
          style={{marginTop: 10, backgroundColor: COLORS.white, padding: 20}}>
          <Text>Pilih Pembayaran</Text>
          <ScrollView horizontal showsHorizontalScrollIndicator={false}>
            <View style={{flexDirection: 'row'}}>
              <TouchableOpacity onPress={() => setSelection('Bank Transfer')}>
                <View
                  style={{
                    backgroundColor:
                      isSelected == 'Bank Transfer' ? '#03426229' : 'white',
                    height: 82,
                    width: 162,
                    padding: 20,
                    alignItems: 'center',
                    justifyContent: 'center',
                    marginTop: 10,
                    borderColor:
                      isSelected == 'Bank Transfer' ? '#034262' : '#E1E1E1',
                    borderWidth: 1,
                    marginHorizontal: 5,
                    borderRadius: 10,
                  }}>
                  {isSelected == 'Bank Transfer' ? (
                    <View style={{position: 'absolute', right: 0, top: 0}}>
                      <Icon
                        name="ios-checkmark-circle"
                        size={22}
                        color="#034262"
                      />
                    </View>
                  ) : null}
                  <Image source={require('../assets/image/ovo.png')} />
                  <Text>Bank Transfer</Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => setSelection('OVO')}>
                <View
                  style={{
                    backgroundColor:
                      isSelected == 'OVO' ? '#03426229' : 'white',
                    height: 82,
                    width: 162,
                    padding: 20,
                    alignItems: 'center',
                    justifyContent: 'center',
                    marginTop: 10,
                    borderColor: isSelected == 'OVO' ? '#034262' : '#E1E1E1',
                    borderWidth: 1,
                    marginHorizontal: 5,
                    borderRadius: 10,
                  }}>
                  {isSelected == 'OVO' ? (
                    <View style={{position: 'absolute', right: 0, top: 0}}>
                      <Icon
                        name="ios-checkmark-circle"
                        size={22}
                        color="#034262"
                      />
                    </View>
                  ) : null}
                  <Image source={require('../assets/image/ovo.png')} />
                  <Text>OVO</Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => setSelection('E-Payment')}>
                <View
                  style={{
                    backgroundColor:
                      isSelected == 'E-Payment' ? '#03426229' : 'white',
                    height: 82,
                    width: 162,
                    padding: 20,
                    alignItems: 'center',
                    justifyContent: 'center',
                    marginTop: 10,
                    borderColor:
                      isSelected == 'E-Payment' ? '#034262' : '#E1E1E1',
                    borderWidth: 1,
                    marginHorizontal: 5,
                    borderRadius: 10,
                  }}>
                  {isSelected == 'E-Payment' ? (
                    <View style={{position: 'absolute', right: 0, top: 0}}>
                      <Icon
                        name="ios-checkmark-circle"
                        size={22}
                        color="#034262"
                      />
                    </View>
                  ) : null}
                  <Image source={require('../assets/image/ovo.png')} />
                  <Text>E-Payment</Text>
                </View>
              </TouchableOpacity>
            </View>
          </ScrollView>

          {/* <FlatList
            data={listPembayaran}
            renderItem={({item}) => <SliderItem item={item} />}
            horizontal
            pagingEnabled
            snapToAlignment="center"
            showsHorizontalScrollIndicator={false}
          /> */}
        </View>
        <View
          style={{
            marginTop: 10,
            backgroundColor: COLORS.white,
            paddingHorizontal: 22,
          }}>
          <TouchableOpacity
            style={styles.botton}
            onPress={() => navigation.navigate('Summary')}>
            <Text
              style={{color: COLORS.white, fontSize: 16, fontWeight: 'bold'}}>
              Pesan Sekarang
            </Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </View>
  );
};

export default Checkout;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLORS.silver,
  },
  customer: {
    backgroundColor: COLORS.white,
    padding: 20,
  },
  outlet: {
    backgroundColor: COLORS.white,
    marginTop: 10,
    padding: 20,
  },
  barang: {
    // height: 135,
    width: '100%',
    flexDirection: 'row',
    // backgroundColor: 'red',
    marginTop: 10,
    alignItems: 'center',
    marginLeft: -10,
    // borderRadius: 10,
  },
  rincian: {
    marginTop: 10,
    backgroundColor: COLORS.white,
    padding: 20,
  },
  botton: {
    backgroundColor: COLORS.primary,
    justifyContent: 'center',
    alignItems: 'center',
    height: 55,
    width: '100%',
    borderRadius: 10,
    marginTop: 20,
  },
});
