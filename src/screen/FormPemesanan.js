import {
  Image,
  KeyboardAvoidingView,
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import React, {useState} from 'react';
import {COLORS} from '../constant';
import Icon from 'react-native-vector-icons/dist/Ionicons';
import DocumentPicker from 'react-native-document-picker';
// import {CheckBox} from 'react-native-elements';
const FormPemesanan = ({navigation, route}) => {
  const [isSelected, setSelection] = useState('');
  const [merk, setMerk] = useState('');
  const [color, setColor] = useState('');
  const [size, setSize] = useState('');
  const [note, setNote] = useState('');
  const [kupon, setKupon] = useState('');
  const [singleFile, setSingleFile] = useState(null);

  const dataKupon = route.params;

  const addToCart = () => {
    // alert(merk + color + size + isSelected + note + singleFile);
    const formData = {
      checkbox: isSelected,
      merk,
      color,
      size,
      note,
      img: singleFile,
      kupon: dataKupon.title,
    };
    console.log('res1234', formData);

    navigation.navigate('Keranjang', formData);
  };

  const selectFile = async () => {
    // Opening Document Picker to select one file
    try {
      const res = await DocumentPicker.pick({
        // Provide which type of file you want user to pick
        type: [DocumentPicker.types.allFiles],
        // There can me more options as well
        // DocumentPicker.types.allFiles
        // DocumentPicker.types.images
        // DocumentPicker.types.plainText
        // DocumentPicker.types.audio
        // DocumentPicker.types.pdf
      });
      // Printing the log realted to the file
      console.log('res1234 : ' + JSON.stringify(res));
      console.log('res1234 photo', res);
      // Setting the state to show single file attributes
      setSingleFile(res[0].uri);
    } catch (err) {
      setSingleFile(null);
      // Handling any exception (If any)
      if (DocumentPicker.isCancel(err)) {
        // If user canceled the document selection
        alert('Canceled');
      } else {
        // For Unknown Error
        alert('Unknown Error: ' + JSON.stringify(err));
        throw err;
      }
    }
  };

  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <Icon
          name="arrow-back"
          size={22}
          color="black"
          onPress={() => navigation.pop()}
        />
        <Text
          style={{
            color: COLORS.black,
            marginLeft: 5,
            fontSize: 18,
            fontWeight: 'bold',
          }}>
          Formulir Pemesanan
        </Text>
      </View>
      <ScrollView
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{paddingBottom: 20}}>
        <KeyboardAvoidingView enable keyboardVerticalOffset={-500}>
          <View style={{paddingHorizontal: 22}}>
            <Text style={styles.tittleInput}>Merek</Text>
            <TextInput
              placeholder="Masukan Merk Barang"
              value={merk}
              onChangeText={text => setMerk(text)}
              style={styles.textInput}
            />
            <Text style={styles.tittleInput}>Warna</Text>
            <TextInput
              placeholder="Warna Barang, cth : Merah - Putih "
              value={color}
              onChangeText={text => setColor(text)}
              style={styles.textInput}
            />
            <Text style={styles.tittleInput}>Ukuran</Text>
            <TextInput
              value={size}
              onChangeText={text => setSize(text)}
              placeholder="Cth : S, M, L / 39,40"
              style={styles.textInput}
            />
            <Text style={styles.tittleInput}>Photo</Text>
            <TouchableOpacity style={styles.inputImg} onPress={selectFile}>
              <Icon
                name="ios-camera-outline"
                size={22}
                color={COLORS.primary}
                // style={{marginTop: 20}}
              />
              <Text
                style={{
                  color: COLORS.primary,
                  marginTop: 10,
                  // width: 84,
                  // height: 84,
                }}>
                Add Photo
              </Text>
              {/* {singleFile != null ? (
                // <Image source={{uri: singleFile.uri}} />
                <Text>URI: {singleFile.uri ? singleFile.uri : ''}</Text>
              ) : null} */}
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => setSelection('Ganti Sol Sepatu')}
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                marginVertical: 5,
              }}>
              <View
                style={{
                  height: 24,
                  width: 24,
                  borderColor: '#B8B8B8',
                  borderWidth: 1,
                  borderRadius: 3,
                }}>
                {isSelected == 'Ganti Sol Sepatu' ? (
                  <Icon
                    name="ios-checkmark-sharp"
                    size={22}
                    color={COLORS.primary}
                  />
                ) : null}
              </View>
              <Text style={{marginLeft: 10}}>Ganti Sol Sepatu</Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => setSelection('Jahit Sepatu')}
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                marginVertical: 5,
              }}>
              <View
                style={{
                  height: 24,
                  width: 24,
                  borderColor: '#B8B8B8',
                  borderWidth: 1,
                  borderRadius: 3,
                }}>
                {isSelected == 'Jahit Sepatu' ? (
                  <Icon
                    name="ios-checkmark-sharp"
                    size={22}
                    color={COLORS.primary}
                  />
                ) : null}
              </View>
              <Text style={{marginLeft: 10}}>Jahit Sepatu</Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => setSelection('Repaint Sepatu')}
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                marginVertical: 5,
              }}>
              <View
                style={{
                  height: 24,
                  width: 24,
                  borderColor: '#B8B8B8',
                  borderWidth: 1,
                  borderRadius: 3,
                }}>
                {isSelected == 'Repaint Sepatu' ? (
                  <Icon
                    name="ios-checkmark-sharp"
                    size={22}
                    color={COLORS.primary}
                  />
                ) : null}
              </View>
              <Text style={{marginLeft: 10}}>Repaint Sepatu</Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => setSelection('Cuci Sepatu')}
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                marginVertical: 5,
              }}>
              <View
                style={{
                  height: 24,
                  width: 24,
                  borderColor: '#B8B8B8',
                  borderWidth: 1,
                  borderRadius: 3,
                }}>
                {isSelected == 'Cuci Sepatu' ? (
                  <Icon
                    name="ios-checkmark-sharp"
                    size={22}
                    color={COLORS.primary}
                  />
                ) : null}
              </View>
              <Text style={{marginLeft: 10}}>Cuci Sepatu</Text>
            </TouchableOpacity>
            <Text style={styles.tittleInput}>Catatan</Text>
            <TextInput
              placeholder="Cth : ingin ganti sol baru"
              style={styles.catatan}
              value={note}
              onChangeText={text => setNote(text)}
            />
            <Text style={styles.tittleInput}>Kupon Promo </Text>
            <TouchableOpacity
              onPress={() => navigation.navigate('KodeKupon')}
              style={{
                borderColor: COLORS.primary,
                borderWidth: 2,
                height: 47,
                width: '100%',
                marginTop: 10,
                justifyContent: 'center',
                paddingHorizontal: 15,
                borderRadius: 8,
                // backgroundColor: 'black',
              }}>
              {dataKupon ? (
                <View style={{flexDirection: 'row'}}>
                  <Text style={{marginRight: 10, color: 'black'}}>
                    {dataKupon.title}
                  </Text>
                  <Icon name="ios-checkmark-circle" size={22} color="#11A84E" />
                </View>
              ) : (
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                  }}>
                  <Text>Pilih Kupon Promo</Text>
                  <Icon
                    name="chevron-forward"
                    size={22}
                    color={COLORS.primary}
                  />
                </View>
              )}
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.bottomconfirm}
              onPress={() => addToCart()}>
              <Text style={styles.textbottom}>Masukkan Keranjang</Text>
            </TouchableOpacity>
          </View>
        </KeyboardAvoidingView>
      </ScrollView>
    </View>
  );
};

export default FormPemesanan;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLORS.white,
  },
  header: {
    height: 56,
    width: '100%',
    backgroundColor: COLORS.white,
    // justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    paddingHorizontal: 22,
    shadowColor: COLORS.black,
    shadowOffset: {
      height: 2,
      width: 0,
    },
    shadowOpacity: 0.25,
    elevation: 5,
  },
  tittleInput: {
    color: COLORS.primary,
    fontSize: 16,
    fontWeight: 'bold',
    marginTop: 25,
  },
  textInput: {
    backgroundColor: COLORS.silver,
    // position: 'absolute',
    width: '100%',
    height: 45,
    marginTop: 11,
    paddingLeft: 10,
    borderRadius: 10,
  },
  inputImg: {
    backgroundColor: COLORS.white,
    alignSelf: 'flex-start',
    padding: 10,
    borderRadius: 10,
    // height: 84,
    // width: 84,
    // justifyContent: 'center',
    alignItems: 'center',
    marginTop: 14,
    marginBottom: 40,
    borderColor: COLORS.primary,
    borderWidth: 1,
  },
  catatan: {
    backgroundColor: COLORS.silver,
    // position: 'absolute',
    width: '100%',
    height: 100,
    marginTop: 11,
    paddingLeft: 10,
    borderRadius: 10,
  },
  bottomconfirm: {
    marginTop: 30,
    backgroundColor: COLORS.primary,
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 8,
    paddingVertical: 15,
  },
  textbottom: {
    color: COLORS.white,
    fontSize: 16,
    fontWeight: 'bold',
  },
});
