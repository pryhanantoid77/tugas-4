import {
  Image,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import React from 'react';
import {COLORS} from '../constant';

const DetailPromo = ({navigation, route}) => {
  const dataKupon = route.params;
  return (
    <View style={styles.container}>
      <ScrollView
        contentContainerStyle={{paddingBottom: 20}}
        showsVerticalScrollIndicator={false}>
        <View
          style={{
            backgroundColor: COLORS.white,
            flexDirection: 'row',
            alignItems: 'center',
            padding: 30,
          }}>
          <View
            style={{
              backgroundColor: '#FFDFE0',
              height: 80,
              width: 80,
              borderRadius: 50,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Image source={dataKupon.img} />
          </View>
          <Text
            style={{
              fontWeight: 'bold',
              marginLeft: 10,
              fontSize: 16,
              color: 'black',
            }}>
            {dataKupon.title}
          </Text>
        </View>
        <View style={{backgroundColor: 'white', marginTop: 10, padding: 22}}>
          <Text style={{color: 'black', fontWeight: 'bold', marginBottom: 20}}>
            Ketentuan Promo
          </Text>
          <Text style={{color: 'black'}}>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Dictum hac
            ut id tincidunt egestas nulla. Turpis curabitur risus lobortis eget.
            Venenatis, nulla amet, amet diam aliquam dictum a ligula. Quam
            rhoncus malesuada turpis quam.
          </Text>
          <Text style={{color: 'black', marginTop: 10}}>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Dictum hac
            ut id tincidunt egestas nulla. Turpis curabitur risus lobortis eget.
            Venenatis, nulla amet, amet diam aliquam dictum a ligula. Quam
            rhoncus malesuada turpis quam.
          </Text>
          <Text style={{color: 'black', marginTop: 10}}>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Dictum hac
            ut id tincidunt egestas nulla. Turpis curabitur risus lobortis eget.
            Venenatis, nulla amet, amet diam aliquam dictum a ligula. Quam
            rhoncus malesuada turpis quam.
          </Text>
        </View>
        <View style={{paddingHorizontal: 22}}>
          <TouchableOpacity
            style={styles.bottomconfirm}
            onPress={() => navigation.navigate('FormPemesanan', dataKupon)}>
            <Text style={styles.textbottom}>Gunakan Promo</Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </View>
  );
};

export default DetailPromo;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLORS.silver,
  },
  bottomconfirm: {
    marginTop: 30,
    backgroundColor: COLORS.primary,
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 8,
    paddingVertical: 15,
  },
  textbottom: {
    color: COLORS.white,
    fontSize: 16,
    fontWeight: 'bold',
  },
});
